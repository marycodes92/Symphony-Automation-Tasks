# Symphony-Automation-Tasks
This project contains API tests for "https://api.publicapis.org/entries" and end to end tests the sauce demo application.

## About the project
The page object model is used to ensure readablity, reusability and easy maintenance of the code. 
The CI/CD pipeline runs once a commit is made and an email is sent to me once it succeeds or fails.

## Getting Started 
1. Clone the repository "https://gitlab.com/marycodes92/Symphony-Automation-Tasks.git"
2. Open in your choice IDE, prefarably Visual Studio code (Vs Code)
3. Run "npm install" to install dependencies 

## Running the Tests 
To run the api test file only. 
Run "npm run test:api"

To run the e2e test file;
Run npm run test:e2e:ci

# Debug Mode 
Run "npm run test:e2e

## Run all tests 
Run "npm run test"

## Reporting
The reporter used in this project is the github Action reporter. The report are added to the allure report folder created once a run is completed.